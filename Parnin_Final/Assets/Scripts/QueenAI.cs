﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueenAI : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    public int QHealth;
    void Start()
    {
        InvokeRepeating("Shoot", .5f, .5f); // delay bullet spwan

    }
   
    void Shoot()
    {
        int PlayerMask = LayerMask.GetMask("Player"); //refrence layer

        if (Physics2D.Raycast(transform.position, Vector2.left, 30f, PlayerMask))// check to to see if hit player layer
        {

            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.left), Color.yellow);
            Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);// spwan bullet

        }



    }

}
