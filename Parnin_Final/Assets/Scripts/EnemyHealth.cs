﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health = 3;
    public GameObject FuelPrefab;

    public void TakeDamage( int damageAmount)
    {
        health -= damageAmount;// subtract from current health

        if (health <= 0)
        {
            Instantiate(FuelPrefab);// spwan bullet

            Destroy(gameObject);
        
        }
    }
}
