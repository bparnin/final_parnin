﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLeft : MonoBehaviour
{

    public float speed = 20f;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = -transform.right * speed;
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, .5f); // destory object after 5sec
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.isTrigger)
        {

            if (other.gameObject.CompareTag("Enemy"))
            {

                EnemyHealth eHealth = other.gameObject.GetComponent<EnemyHealth>();

                if (eHealth != null)
                    eHealth.TakeDamage(1); // enemy takes a damage
            }
            Destroy(gameObject);// destory bullet 
        }
    }

}
