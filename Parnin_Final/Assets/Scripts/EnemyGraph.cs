﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyGraph : MonoBehaviour
{
    public AIPath aiPath; // refrence to the AIPath I am using

    // Update is called once per frame
    void Update()
    {
        if (aiPath.desiredVelocity.x >= .01f)// if positive 
        {

            transform.localScale = new Vector3(1f, 1f, 1f);// then the sprite faces right
        }else if (aiPath.desiredVelocity.x <= -.01f) // if negative velocity
        {
            transform.localScale = new Vector3(-1f, 1f, 1f); // then sprite faces left

        }
    }
}
