﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemeyBulletRight : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        //rb.velocity = transform.right * speed;
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 5f); // destory object after 5sec
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.isTrigger)
        {

            if (other.gameObject.CompareTag("Player"))
            {

                PlayerMove eHealth = other.gameObject.GetComponent<PlayerMove>();// get player move scrip and player health in script

                if (eHealth != null)
                    eHealth.TakeDamage(2); // Player takes a damage
            }
            Destroy(gameObject);// destory bullet 
        }
    }
}
