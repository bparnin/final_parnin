﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;


public class PlayerMove : MonoBehaviour
{
    Rigidbody2D rB2D;
    public float runSpeed;
    public float jumpSpeed;
    public float JetSpeed;
    public float fuel;
    public int health = 3;
    public TextMeshProUGUI HealthText; // Health text
    public TextMeshProUGUI JetText;
    public GameObject YouWinText;
    public SpriteRenderer spriteRenderer;
    public GameObject M_Blowup;
    public Camera Cam;
    public Transform firePoint;
    public Transform firePointLeft;
    public GameObject bulletPrefab;
    public GameObject bulletLeftPrefab;
    public Animator animator;

    bool isright;
    Vector2 mousePos;

    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        SetHealthText(); // set health to text
        SetJetText();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && fuel > 0)
        {
            Jet();

        }
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask)) // if boxcast hit level mask
            {
                Jump();
            }
        }
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
        //mousePos = Cam.ScreenToWorldPoint(Input.mousePosition); // locates mouse position in game
    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0f)// player us going right
        {
            spriteRenderer.flipX = false; //flip player
            isright = true;
        }
        else
       if (rB2D.velocity.x < 0f) // player going left
        {
            spriteRenderer.flipX = true;// flip sprite
            isright = false; 
        }

       // Vector2 aimDir = mousePos - rB2D.position;// create a vection between postion and mousepostion
        //float angle = Mathf.Atan2(aimDir.y, aimDir.x) * Mathf.Rad2Deg;// get angle to the vector
        //FirePoint.transform = angle;

        if (Mathf.Abs(horizontalInput) > 0f) // if horizontal input is greater than 0f
        {

            animator.SetBool("IsRunning", true); //player is running
            
        }
        else
            animator.SetBool("IsRunning", false);
    }
    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpSpeed);

    }

    void Jet()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, JetSpeed);
        fuel--;
        SetJetText();
    }

    void SetJetText()
    {
        JetText.text = "Jet Fuel:" + fuel.ToString();
    }
    void SetHealthText()
    {
        HealthText.text = "Health: " + health.ToString(); // add   health to canvas
    }
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.CompareTag("KillZone"))
        {
            GameOver();
        }
        if (other.gameObject.CompareTag("Stage1")) //if player enter first stage
        {
            M_Blowup.SetActive(true); // enable enemies
        }
        if (other.gameObject.CompareTag("End"))
        {
            YouWinText.SetActive(true);
        }
        if (other.gameObject.CompareTag("Fuel"))
        {
            fuel  ++;
            SetJetText();
            other.gameObject.SetActive(false);
        }
    }
    

    private void OnCollisionEnter2D(Collision2D other)

    {     
    
        if (other.gameObject.CompareTag("Hazard")) //if player hits hazard
        {
            TakeDamage(5);
        }

    }
    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;// subtract from current health
        SetHealthText();

        if (health <= 0)
        {

            GameOver();

        }
    }
    void Shoot()
        {
        if (isright == true)
        {
            Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        }
        if (isright ==false)
        {
            Instantiate(bulletLeftPrefab, firePointLeft.position, firePointLeft.rotation);

        }
    }
    
    private void GameOver()
    {
        SceneManager.LoadScene(0); //reload scene 
    }
}