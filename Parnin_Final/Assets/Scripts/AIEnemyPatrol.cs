﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemyPatrol : MonoBehaviour
{
    public Transform firePoint;
    public Transform fireRight;
    public GameObject bulletPrefab;
    public GameObject bullet2Prefab;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating ("Shoot", 1f,1f);

    }
    void Shoot()
    {
        int PlayerMask = LayerMask.GetMask("Player");

        if (Physics2D.Raycast(transform.position, Vector2.down, 15f, PlayerMask))
        {
            print("hit");
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.left), Color.yellow);
            Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

        }

        if (Physics2D.Raycast(transform.position, Vector2.down, 5f, PlayerMask))
        {
            print("righthit");
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.right), Color.yellow);
            Instantiate(bullet2Prefab, fireRight.position, fireRight.rotation);

        }

    }

// Update is called once per frame
void Update()
    {
        //Vector2 right = transform.TransformDirection(Vector2.right);

       // if (Physics2D.Raycast(transform.position, right, 50))
           // print("There is something in front of the object!");

    }

    private void FixedUpdate()
    {



       

    }
}
